class CreatePages < ActiveRecord::Migration[5.0]
  def change
    create_table :pages do |t|
      t.string :name
      t.string :h1
      t.string :h2
      t.boolean :display

      t.timestamps
    end
  end
end
