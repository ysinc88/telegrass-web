class PagesController < ApplicationController
before_action :set_page, only: [:update, :show]

  def landing
    @page = Page.find_by_name("landing")
  end

  def show

  end
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to @page, notice: 'Test was successfully updated.' }
        format.json { respond_with_bip(@page) }
      else
        format.html { render :edit }
        format.json { respond_with_bip(@page) }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_page
    @page = Page.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def page_params
    params.require(:page).permit(:h1, :h2, :display, :name)
  end
end
